﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Assignment1
{
    public partial class AssignmentGraderForm : Form
    {
        #region Data members

        private const int functionalityTabValue = 0;

        private const int implementationTabValue = 1;

        private const int documentationTabValue = 2;

        #endregion

        #region Properties

        public List<string[]> AllComments { get; set; }

        public static int FunctionalityTabValue => functionalityTabValue;

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="AssignmentGraderForm" /> class.
        /// </summary>
        public AssignmentGraderForm()
        {
            this.InitializeComponent();

            this.AllComments = new List<string[]>();

            this.initializeComments();

            this.functionalityGraderControl.ControlModified += this.handleGraderControlModified;

            this.implementationGraderControl.ControlModified += this.handleGraderControlModified;

            this.documentationGraderControl.ControlModified += this.handleGraderControlModified;
        }

        #endregion

        #region Methods

        private void initializeComments()
        {
            this.initializeGraderControl(this.functionalityGraderControl, "Well done.", "No issues were discovered.");

            this.initializeGraderControl(this.implementationGraderControl, "Magic numbers are used within the code.",
                "Overall class design not following best practices.", "Violations of DRY need addressed.");

            this.initializeGraderControl(this.documentationGraderControl, "Good job.",
                "Missing lots of required documentation");
        }

        private void initializeGraderControl(CommentGraderUserControl.CommentGraderUserControl graderControl,
            params string[] comments)
        {
            graderControl.BuildRadioButtonText();
            foreach (var current in comments)
            {
                graderControl.AddComment(current);
            }
        }

        private void handleGraderControlModified(object sender,
            CommentGraderUserControl.CommentGraderUserControl.ControlModifiedEventArgs e)
        {
            var theSender = sender as CommentGraderUserControl.CommentGraderUserControl;
            this.updateSummaryOutput();
        }

        private void updateSummaryOutput()
        {
            var maxPoints = this.calculateMaxPoints();
            var summaryOutput = new SummaryBuilder(maxPoints, this.functionalityGraderControl,
                this.implementationGraderControl, this.documentationGraderControl);
            this.summaryTextBox.Text = summaryOutput.BuildSummary().ToString();
        }

        private void copyButton_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(this.summaryTextBox.Text);
        }

        private void clearButton_Click(object sender, EventArgs e)
        {
            this.summaryTextBox.Text = string.Empty;

            this.resetGraderControl(this.functionalityGraderControl);
            this.resetGraderControl(this.implementationGraderControl);
            this.resetGraderControl(this.documentationGraderControl);
        }

        private int calculateMaxPoints()
        {
            return this.functionalityGraderControl.ExceptionalValue +
                   this.implementationGraderControl.ExceptionalValue + this.documentationGraderControl.ExceptionalValue;
        }

        private void resetGraderControl(CommentGraderUserControl.CommentGraderUserControl graderControl)
        {
            graderControl.CheckAllStatus(false);
            graderControl.ResetButtons();
        }

        private void populateAllComments()
        {
            this.findControlComments(this.functionalityGraderControl);
            this.findControlComments(this.implementationGraderControl);
            this.findControlComments(this.documentationGraderControl);
        }

        private void findControlComments(CommentGraderUserControl.CommentGraderUserControl graderControl)
        {
            var comments = graderControl.ExtractComments(false);
            this.AllComments.Add(comments);
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.populateAllComments();
            var xmlSerializer = new XmlSerializer(typeof(List<string[]>));
            var path = Environment.CurrentDirectory + "../saveFile.xml";
            var file = File.Create(path);

            xmlSerializer.Serialize(file, this.AllComments);
            file.Close();
        }

        private void loadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var file = this.findSaveFile();

            if (file != null)
            {
                var xmlSerializer = new XmlSerializer(typeof(List<string[]>));
                var data = xmlSerializer.Deserialize(file);
                this.AllComments = (List<string[]>) data;
                this.loadCommentsToControls(this.AllComments);
            }
        }

        private FileStream findSaveFile()
        {
            try
            {
                var path = Environment.CurrentDirectory + "../saveFile.xml";
                var file = File.OpenRead(path);
                return file;
            }
            catch (FileNotFoundException fnfe)
            {
                this.summaryTextBox.Text = fnfe.Message + Environment.NewLine + Environment.NewLine +
                                           "Previous save data might not exist.";
            }
            return null;
        }

        private void loadCommentsToControls(List<string[]> data)
        {
            this.functionalityGraderControl.PopulateTextCells(data[functionalityTabValue]);
            this.implementationGraderControl.PopulateTextCells(data[implementationTabValue]);
            this.documentationGraderControl.PopulateTextCells(data[documentationTabValue]);
        }

        #endregion
    }
}