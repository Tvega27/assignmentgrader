﻿using System.Text;

namespace Assignment1
{
    public class SummaryBuilder
    {
        #region Properties

        /// <summary>
        ///     Gets or sets the maximum points.
        /// </summary>
        /// <value>
        ///     The maximum points.
        /// </value>
        public int MaxPoints { get; set; }

        /// <summary>
        ///     Gets or sets the functionality control.
        /// </summary>
        /// <value>
        ///     The functionality control.
        /// </value>
        public CommentGraderUserControl.CommentGraderUserControl FunctionalityControl { get; set; }

        /// <summary>
        ///     Gets or sets the implementation control.
        /// </summary>
        /// <value>
        ///     The implementation control.
        /// </value>
        public CommentGraderUserControl.CommentGraderUserControl ImplementationControl { get; set; }

        /// <summary>
        ///     Gets or sets the documentation control.
        /// </summary>
        /// <value>
        ///     The documentation control.
        /// </value>
        public CommentGraderUserControl.CommentGraderUserControl DocumentationControl { get; set; }

        /// <summary>
        ///     Gets the functionality maximum value.
        /// </summary>
        /// <value>
        ///     The functionality maximum value.
        /// </value>
        public int FunctionalityMaxValue => this.FunctionalityControl.ExceptionalValue;

        /// <summary>
        ///     Gets the functionality actual value.
        /// </summary>
        /// <value>
        ///     The functionality actual value.
        /// </value>
        public int FunctionalityActualValue => this.FunctionalityControl.FindButtonChosenValue();

        /// <summary>
        ///     Gets the functionality control comments.
        /// </summary>
        /// <value>
        ///     The functionality control comments.
        /// </value>
        public string[] FunctionalityComments => this.FunctionalityControl.ExtractComments(true);

        /// <summary>
        ///     Gets the implementation maximum value.
        /// </summary>
        /// <value>
        ///     The implementation maximum value.
        /// </value>
        public int ImplementationMaxValue => this.ImplementationControl.ExceptionalValue;

        /// <summary>
        ///     Gets the implementation actual value.
        /// </summary>
        /// <value>
        ///     The implementation actual value.
        /// </value>
        public int ImplementationActualValue => this.ImplementationControl.FindButtonChosenValue();

        /// <summary>
        ///     Gets the implementation control comments.
        /// </summary>
        /// <value>
        ///     The implementation control comments.
        /// </value>
        public string[] ImplementationComments => this.ImplementationControl.ExtractComments(true);

        /// <summary>
        ///     Gets the documentation maximum value.
        /// </summary>
        /// <value>
        ///     The documentation maximum value.
        /// </value>
        public int DocumentationMaxValue => this.DocumentationControl.ExceptionalValue;

        /// <summary>
        ///     Gets the documentation actual value.
        /// </summary>
        /// <value>
        ///     The documentation actual value.
        /// </value>
        public int DocumentationActualValue => this.DocumentationControl.FindButtonChosenValue();

        /// <summary>
        ///     Gets the documentation control comments.
        /// </summary>
        /// <value>
        ///     The documentation control comments.
        /// </value>
        public string[] DocumentationComments => this.DocumentationControl.ExtractComments(true);

        /// <summary>
        ///     Gets or sets the actual points.
        /// </summary>
        /// <value>
        ///     The actual points.
        /// </value>
        public int ActualPoints => this.FunctionalityActualValue + this.ImplementationActualValue +
                                   this.DocumentationActualValue;

        #endregion

        #region Constructors

        public SummaryBuilder(int maxPoints, CommentGraderUserControl.CommentGraderUserControl functionalityControl,
            CommentGraderUserControl.CommentGraderUserControl implementationControl,
            CommentGraderUserControl.CommentGraderUserControl documentationControl)
        {
            this.MaxPoints = maxPoints;
            this.FunctionalityControl = functionalityControl;
            this.ImplementationControl = implementationControl;
            this.DocumentationControl = documentationControl;
        }

        #endregion

        #region Methods

        public StringBuilder BuildSummary()
        {
            var result = new StringBuilder();

            result.AppendLine("Total: " + this.ActualPoints + "/" + this.MaxPoints);
            result.AppendLine();
            result.AppendLine();

            this.buildSectionOutput(result, "Functionality: ", this.FunctionalityActualValue,
                this.FunctionalityMaxValue, this.FunctionalityComments);

            this.buildSectionOutput(result, "Implementation: ", this.ImplementationActualValue,
                this.ImplementationMaxValue, this.ImplementationComments);

            this.buildSectionOutput(result, "Documentation: ", this.DocumentationActualValue,
                this.DocumentationMaxValue, this.DocumentationComments);

            return result;
        }

        private StringBuilder buildSectionOutput(StringBuilder result, string section, int actualValue, int maxValue,
            string[] comments)
        {
            result.AppendLine(section + actualValue + "/" + maxValue);

            foreach (var currentComment in comments)
            {
                result.AppendLine(currentComment);
            }

            result.AppendLine();
            result.AppendLine();
            return result;
        }

        #endregion
    }
}