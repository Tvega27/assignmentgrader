﻿namespace Assignment1
{
    partial class AssignmentGraderForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.graderTabControl = new System.Windows.Forms.TabControl();
            this.functionalityTab = new System.Windows.Forms.TabPage();
            this.implementationTab = new System.Windows.Forms.TabPage();
            this.documentationTab = new System.Windows.Forms.TabPage();
            this.summaryTextBox = new System.Windows.Forms.TextBox();
            this.copyButton = new System.Windows.Forms.Button();
            this.clearButton = new System.Windows.Forms.Button();
            this.sumamryLabel = new System.Windows.Forms.Label();
            this.CommentsMenuStrip = new System.Windows.Forms.MenuStrip();
            this.commentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.loadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.functionalityGraderControl = new CommentGraderUserControl.CommentGraderUserControl();
            this.implementationGraderControl = new CommentGraderUserControl.CommentGraderUserControl();
            this.documentationGraderControl = new CommentGraderUserControl.CommentGraderUserControl();
            this.graderTabControl.SuspendLayout();
            this.functionalityTab.SuspendLayout();
            this.implementationTab.SuspendLayout();
            this.documentationTab.SuspendLayout();
            this.CommentsMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // graderTabControl
            // 
            this.graderTabControl.Controls.Add(this.functionalityTab);
            this.graderTabControl.Controls.Add(this.implementationTab);
            this.graderTabControl.Controls.Add(this.documentationTab);
            this.graderTabControl.Location = new System.Drawing.Point(16, 49);
            this.graderTabControl.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.graderTabControl.Name = "graderTabControl";
            this.graderTabControl.SelectedIndex = 0;
            this.graderTabControl.Size = new System.Drawing.Size(747, 384);
            this.graderTabControl.TabIndex = 1;
            // 
            // functionalityTab
            // 
            this.functionalityTab.Controls.Add(this.functionalityGraderControl);
            this.functionalityTab.Location = new System.Drawing.Point(4, 25);
            this.functionalityTab.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.functionalityTab.Name = "functionalityTab";
            this.functionalityTab.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.functionalityTab.Size = new System.Drawing.Size(739, 355);
            this.functionalityTab.TabIndex = 0;
            this.functionalityTab.Text = "Functionality";
            this.functionalityTab.UseVisualStyleBackColor = true;
            // 
            // implementationTab
            // 
            this.implementationTab.Controls.Add(this.implementationGraderControl);
            this.implementationTab.Location = new System.Drawing.Point(4, 25);
            this.implementationTab.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.implementationTab.Name = "implementationTab";
            this.implementationTab.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.implementationTab.Size = new System.Drawing.Size(739, 355);
            this.implementationTab.TabIndex = 1;
            this.implementationTab.Text = "Implementation";
            this.implementationTab.UseVisualStyleBackColor = true;
            // 
            // documentationTab
            // 
            this.documentationTab.Controls.Add(this.documentationGraderControl);
            this.documentationTab.Location = new System.Drawing.Point(4, 25);
            this.documentationTab.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.documentationTab.Name = "documentationTab";
            this.documentationTab.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.documentationTab.Size = new System.Drawing.Size(739, 355);
            this.documentationTab.TabIndex = 2;
            this.documentationTab.Text = "Documentation";
            this.documentationTab.UseVisualStyleBackColor = true;
            // 
            // summaryTextBox
            // 
            this.summaryTextBox.Location = new System.Drawing.Point(16, 484);
            this.summaryTextBox.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.summaryTextBox.Multiline = true;
            this.summaryTextBox.Name = "summaryTextBox";
            this.summaryTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.summaryTextBox.Size = new System.Drawing.Size(740, 216);
            this.summaryTextBox.TabIndex = 2;
            // 
            // copyButton
            // 
            this.copyButton.Location = new System.Drawing.Point(233, 708);
            this.copyButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.copyButton.Name = "copyButton";
            this.copyButton.Size = new System.Drawing.Size(147, 28);
            this.copyButton.TabIndex = 3;
            this.copyButton.Text = "Copy";
            this.copyButton.UseVisualStyleBackColor = true;
            this.copyButton.Click += new System.EventHandler(this.copyButton_Click);
            // 
            // clearButton
            // 
            this.clearButton.Location = new System.Drawing.Point(400, 708);
            this.clearButton.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.clearButton.Name = "clearButton";
            this.clearButton.Size = new System.Drawing.Size(147, 28);
            this.clearButton.TabIndex = 4;
            this.clearButton.Text = "Clear";
            this.clearButton.UseVisualStyleBackColor = true;
            this.clearButton.Click += new System.EventHandler(this.clearButton_Click);
            // 
            // sumamryLabel
            // 
            this.sumamryLabel.AutoSize = true;
            this.sumamryLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.sumamryLabel.Location = new System.Drawing.Point(17, 450);
            this.sumamryLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.sumamryLabel.Name = "sumamryLabel";
            this.sumamryLabel.Size = new System.Drawing.Size(156, 20);
            this.sumamryLabel.TabIndex = 0;
            this.sumamryLabel.Text = "Grading Breakdown";
            // 
            // CommentsMenuStrip
            // 
            this.CommentsMenuStrip.BackColor = System.Drawing.SystemColors.MenuBar;
            this.CommentsMenuStrip.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.CommentsMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.commentsToolStripMenuItem});
            this.CommentsMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.CommentsMenuStrip.Name = "CommentsMenuStrip";
            this.CommentsMenuStrip.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.CommentsMenuStrip.Size = new System.Drawing.Size(779, 28);
            this.CommentsMenuStrip.TabIndex = 6;
            this.CommentsMenuStrip.Text = "menuStrip1";
            // 
            // commentsToolStripMenuItem
            // 
            this.commentsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadToolStripMenuItem,
            this.saveToolStripMenuItem});
            this.commentsToolStripMenuItem.Name = "commentsToolStripMenuItem";
            this.commentsToolStripMenuItem.Size = new System.Drawing.Size(92, 24);
            this.commentsToolStripMenuItem.Text = "&Comments";
            // 
            // loadToolStripMenuItem
            // 
            this.loadToolStripMenuItem.Name = "loadToolStripMenuItem";
            this.loadToolStripMenuItem.Size = new System.Drawing.Size(182, 26);
            this.loadToolStripMenuItem.Text = "&Load Last Save";
            this.loadToolStripMenuItem.Click += new System.EventHandler(this.loadToolStripMenuItem_Click);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(182, 26);
            this.saveToolStripMenuItem.Text = "&Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // functionalityGraderControl
            // 
            this.functionalityGraderControl.AcceptableValue = 3;
            this.functionalityGraderControl.AmateurValue = 7;
            this.functionalityGraderControl.BackColor = System.Drawing.Color.Transparent;
            this.functionalityGraderControl.ExceptionalValue = 10;
            this.functionalityGraderControl.Location = new System.Drawing.Point(19, 21);
            this.functionalityGraderControl.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.functionalityGraderControl.Name = "functionalityGraderControl";
            this.functionalityGraderControl.Size = new System.Drawing.Size(700, 308);
            this.functionalityGraderControl.TabIndex = 0;
            this.functionalityGraderControl.Tag = "functionality";
            this.functionalityGraderControl.UnsatisfactoryValue = 0;
            // 
            // implementationGraderControl
            // 
            this.implementationGraderControl.AcceptableValue = 5;
            this.implementationGraderControl.AmateurValue = 3;
            this.implementationGraderControl.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.implementationGraderControl.ExceptionalValue = 7;
            this.implementationGraderControl.Location = new System.Drawing.Point(19, 21);
            this.implementationGraderControl.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.implementationGraderControl.Name = "implementationGraderControl";
            this.implementationGraderControl.Size = new System.Drawing.Size(700, 308);
            this.implementationGraderControl.TabIndex = 0;
            this.implementationGraderControl.Tag = "implementation";
            this.implementationGraderControl.UnsatisfactoryValue = 1;
            // 
            // documentationGraderControl
            // 
            this.documentationGraderControl.AcceptableValue = 2;
            this.documentationGraderControl.AmateurValue = 1;
            this.documentationGraderControl.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.documentationGraderControl.ExceptionalValue = 3;
            this.documentationGraderControl.Location = new System.Drawing.Point(19, 21);
            this.documentationGraderControl.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.documentationGraderControl.Name = "documentationGraderControl";
            this.documentationGraderControl.Size = new System.Drawing.Size(700, 308);
            this.documentationGraderControl.TabIndex = 0;
            this.documentationGraderControl.Tag = "documentation";
            this.documentationGraderControl.UnsatisfactoryValue = 0;
            // 
            // AssignmentGraderForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(779, 752);
            this.Controls.Add(this.sumamryLabel);
            this.Controls.Add(this.clearButton);
            this.Controls.Add(this.copyButton);
            this.Controls.Add(this.summaryTextBox);
            this.Controls.Add(this.graderTabControl);
            this.Controls.Add(this.CommentsMenuStrip);
            this.MainMenuStrip = this.CommentsMenuStrip;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "AssignmentGraderForm";
            this.Text = "Assignment Grader  by Tyler Vega";
            this.graderTabControl.ResumeLayout(false);
            this.functionalityTab.ResumeLayout(false);
            this.implementationTab.ResumeLayout(false);
            this.documentationTab.ResumeLayout(false);
            this.CommentsMenuStrip.ResumeLayout(false);
            this.CommentsMenuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl graderTabControl;
        private System.Windows.Forms.TabPage functionalityTab;
        private System.Windows.Forms.TabPage implementationTab;
        private System.Windows.Forms.TabPage documentationTab;
        private CommentGraderUserControl.CommentGraderUserControl functionalityGraderControl;
        private CommentGraderUserControl.CommentGraderUserControl implementationGraderControl;
        private CommentGraderUserControl.CommentGraderUserControl documentationGraderControl;
        private System.Windows.Forms.TextBox summaryTextBox;
        private System.Windows.Forms.Button copyButton;
        private System.Windows.Forms.Button clearButton;
        private System.Windows.Forms.Label sumamryLabel;
        private System.Windows.Forms.MenuStrip CommentsMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem commentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem loadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
    }
}

