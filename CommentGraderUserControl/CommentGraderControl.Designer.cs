﻿namespace CommentGraderUserControl
{
    partial class CommentGraderUserControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.exceptionalRadioButton = new System.Windows.Forms.RadioButton();
            this.acceptableRadioButton = new System.Windows.Forms.RadioButton();
            this.amateurRadioButton = new System.Windows.Forms.RadioButton();
            this.unsatisfactoryRadioButton = new System.Windows.Forms.RadioButton();
            this.commentGraderGridView = new System.Windows.Forms.DataGridView();
            this.addColumn = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.commentColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.gradeRadioButtonGroupBox = new System.Windows.Forms.GroupBox();
            this.checkAllContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.checkAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uncheckAllToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteCellToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.commentGraderGridView)).BeginInit();
            this.gradeRadioButtonGroupBox.SuspendLayout();
            this.checkAllContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // exceptionalRadioButton
            // 
            this.exceptionalRadioButton.AutoSize = true;
            this.exceptionalRadioButton.Checked = true;
            this.exceptionalRadioButton.Location = new System.Drawing.Point(8, 31);
            this.exceptionalRadioButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.exceptionalRadioButton.Name = "exceptionalRadioButton";
            this.exceptionalRadioButton.Size = new System.Drawing.Size(123, 21);
            this.exceptionalRadioButton.TabIndex = 0;
            this.exceptionalRadioButton.TabStop = true;
            this.exceptionalRadioButton.Tag = "exceptional";
            this.exceptionalRadioButton.Text = "(3) Exceptional";
            this.exceptionalRadioButton.UseVisualStyleBackColor = true;
            this.exceptionalRadioButton.CheckedChanged += new System.EventHandler(this.exceptionalRadioButton_CheckedChanged);
            // 
            // acceptableRadioButton
            // 
            this.acceptableRadioButton.AutoSize = true;
            this.acceptableRadioButton.Location = new System.Drawing.Point(8, 57);
            this.acceptableRadioButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.acceptableRadioButton.Name = "acceptableRadioButton";
            this.acceptableRadioButton.Size = new System.Drawing.Size(121, 21);
            this.acceptableRadioButton.TabIndex = 1;
            this.acceptableRadioButton.TabStop = true;
            this.acceptableRadioButton.Tag = "acceptable";
            this.acceptableRadioButton.Text = "(2) Acceptable";
            this.acceptableRadioButton.UseVisualStyleBackColor = true;
            this.acceptableRadioButton.CheckedChanged += new System.EventHandler(this.acceptableRadioButton_CheckedChanged);
            // 
            // amateurRadioButton
            // 
            this.amateurRadioButton.AutoSize = true;
            this.amateurRadioButton.Location = new System.Drawing.Point(8, 84);
            this.amateurRadioButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.amateurRadioButton.Name = "amateurRadioButton";
            this.amateurRadioButton.Size = new System.Drawing.Size(104, 21);
            this.amateurRadioButton.TabIndex = 2;
            this.amateurRadioButton.TabStop = true;
            this.amateurRadioButton.Tag = "amateur";
            this.amateurRadioButton.Text = "(1) Amateur";
            this.amateurRadioButton.UseVisualStyleBackColor = true;
            this.amateurRadioButton.CheckedChanged += new System.EventHandler(this.amateurRadioButton_CheckedChanged);
            // 
            // unsatisfactoryRadioButton
            // 
            this.unsatisfactoryRadioButton.AutoSize = true;
            this.unsatisfactoryRadioButton.Location = new System.Drawing.Point(8, 111);
            this.unsatisfactoryRadioButton.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.unsatisfactoryRadioButton.Name = "unsatisfactoryRadioButton";
            this.unsatisfactoryRadioButton.Size = new System.Drawing.Size(141, 21);
            this.unsatisfactoryRadioButton.TabIndex = 3;
            this.unsatisfactoryRadioButton.TabStop = true;
            this.unsatisfactoryRadioButton.Tag = "unsatisfactory";
            this.unsatisfactoryRadioButton.Text = "(0) Unsatisfactory";
            this.unsatisfactoryRadioButton.UseVisualStyleBackColor = true;
            this.unsatisfactoryRadioButton.CheckedChanged += new System.EventHandler(this.unsatisfactoryRadioButton_CheckedChanged);
            // 
            // commentGraderGridView
            // 
            this.commentGraderGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.commentGraderGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.addColumn,
            this.commentColumn});
            this.commentGraderGridView.Location = new System.Drawing.Point(177, 2);
            this.commentGraderGridView.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.commentGraderGridView.Name = "commentGraderGridView";
            this.commentGraderGridView.RowHeadersVisible = false;
            this.commentGraderGridView.RowTemplate.Height = 24;
            this.commentGraderGridView.Size = new System.Drawing.Size(520, 303);
            this.commentGraderGridView.TabIndex = 4;
            this.commentGraderGridView.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.commentGraderGridView_CellValueChanged);
            this.commentGraderGridView.MouseDown += new System.Windows.Forms.MouseEventHandler(this.commentGraderGridView_MouseDown);
            // 
            // addColumn
            // 
            this.addColumn.HeaderText = "Add";
            this.addColumn.Name = "addColumn";
            this.addColumn.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.addColumn.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.addColumn.Width = 50;
            // 
            // commentColumn
            // 
            this.commentColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.commentColumn.HeaderText = "Comment";
            this.commentColumn.Name = "commentColumn";
            // 
            // gradeRadioButtonGroupBox
            // 
            this.gradeRadioButtonGroupBox.BackColor = System.Drawing.Color.Transparent;
            this.gradeRadioButtonGroupBox.Controls.Add(this.exceptionalRadioButton);
            this.gradeRadioButtonGroupBox.Controls.Add(this.acceptableRadioButton);
            this.gradeRadioButtonGroupBox.Controls.Add(this.unsatisfactoryRadioButton);
            this.gradeRadioButtonGroupBox.Controls.Add(this.amateurRadioButton);
            this.gradeRadioButtonGroupBox.Location = new System.Drawing.Point(13, 2);
            this.gradeRadioButtonGroupBox.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gradeRadioButtonGroupBox.Name = "gradeRadioButtonGroupBox";
            this.gradeRadioButtonGroupBox.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gradeRadioButtonGroupBox.Size = new System.Drawing.Size(157, 148);
            this.gradeRadioButtonGroupBox.TabIndex = 5;
            this.gradeRadioButtonGroupBox.TabStop = false;
            this.gradeRadioButtonGroupBox.Tag = "";
            this.gradeRadioButtonGroupBox.Text = "Grade";
            // 
            // checkAllContextMenu
            // 
            this.checkAllContextMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.checkAllContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.checkAllToolStripMenuItem,
            this.uncheckAllToolStripMenuItem,
            this.deleteCellToolStripMenuItem});
            this.checkAllContextMenu.Name = "checkAllContextMenu";
            this.checkAllContextMenu.Size = new System.Drawing.Size(192, 76);
            // 
            // checkAllToolStripMenuItem
            // 
            this.checkAllToolStripMenuItem.Name = "checkAllToolStripMenuItem";
            this.checkAllToolStripMenuItem.Size = new System.Drawing.Size(175, 24);
            this.checkAllToolStripMenuItem.Text = "&Check All";
            this.checkAllToolStripMenuItem.Click += new System.EventHandler(this.checkAllToolStripMenuItem_Click);
            // 
            // uncheckAllToolStripMenuItem
            // 
            this.uncheckAllToolStripMenuItem.Name = "uncheckAllToolStripMenuItem";
            this.uncheckAllToolStripMenuItem.Size = new System.Drawing.Size(175, 24);
            this.uncheckAllToolStripMenuItem.Text = "&Uncheck All";
            this.uncheckAllToolStripMenuItem.Click += new System.EventHandler(this.uncheckAllToolStripMenuItem_Click);
            // 
            // deleteCellToolStripMenuItem
            // 
            this.deleteCellToolStripMenuItem.Name = "deleteCellToolStripMenuItem";
            this.deleteCellToolStripMenuItem.Size = new System.Drawing.Size(191, 24);
            this.deleteCellToolStripMenuItem.Text = "&Delete Comment";
            this.deleteCellToolStripMenuItem.Click += new System.EventHandler(this.deleteCellToolStripMenuItem_Click);
            // 
            // CommentGraderUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Controls.Add(this.gradeRadioButtonGroupBox);
            this.Controls.Add(this.commentGraderGridView);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "CommentGraderUserControl";
            this.Size = new System.Drawing.Size(700, 308);
            ((System.ComponentModel.ISupportInitialize)(this.commentGraderGridView)).EndInit();
            this.gradeRadioButtonGroupBox.ResumeLayout(false);
            this.gradeRadioButtonGroupBox.PerformLayout();
            this.checkAllContextMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RadioButton exceptionalRadioButton;
        private System.Windows.Forms.RadioButton acceptableRadioButton;
        private System.Windows.Forms.RadioButton amateurRadioButton;
        private System.Windows.Forms.RadioButton unsatisfactoryRadioButton;
        private System.Windows.Forms.DataGridView commentGraderGridView;
        private System.Windows.Forms.DataGridViewCheckBoxColumn addColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn commentColumn;
        private System.Windows.Forms.GroupBox gradeRadioButtonGroupBox;
        private System.Windows.Forms.ContextMenuStrip checkAllContextMenu;
        private System.Windows.Forms.ToolStripMenuItem checkAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uncheckAllToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteCellToolStripMenuItem;
    }
}
