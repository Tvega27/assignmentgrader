﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace CommentGraderUserControl
{
    /// <summary>
    ///     User control for a comment grader. Consists of a radio button group and DataGridView
    ///     for customizable comments.
    /// </summary>
    /// <seealso cref="System.Windows.Forms.UserControl" />
    public partial class CommentGraderUserControl : UserControl
    {
        #region Data members

        /// <summary>
        ///     The control modified event handler
        /// </summary>
        public EventHandler<ControlModifiedEventArgs> ControlModified;

        #endregion

        #region Properties

        /// <summary>
        ///     Gets or sets the exceptional value.
        /// </summary>
        /// <value>
        ///     The exceptional value.
        /// </value>
        public int ExceptionalValue { get; set; }

        /// <summary>
        ///     Gets or sets the acceptable value.
        /// </summary>
        /// <value>
        ///     The acceptable value.
        /// </value>
        public int AcceptableValue { get; set; }

        /// <summary>
        ///     Gets or sets the amateur value.
        /// </summary>
        /// <value>
        ///     The amateur value.
        /// </value>
        public int AmateurValue { get; set; }

        /// <summary>
        ///     Gets or sets the unsatisfactory value.
        /// </summary>
        /// <value>
        ///     The unsatisfactory value.
        /// </value>
        public int UnsatisfactoryValue { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        ///     Initializes a new instance of the <see cref="CommentGraderUserControl" /> class.
        /// </summary>
        public CommentGraderUserControl()
        {
            this.InitializeComponent();
            this.ExceptionalValue = 3;
            this.AcceptableValue = 2;
            this.AmateurValue = 1;
            this.UnsatisfactoryValue = 0;
        }

        #endregion

        #region Methods

        /// <summary>
        ///     Called when [comment grader modified].
        /// </summary>
        /// <param name="commentdDataGridView">The commentd data grid view.</param>
        public void OnCommentGraderModified(DataGridView commentdDataGridView)
        {
            var data = new ControlModifiedEventArgs {CommentDataGridView = commentdDataGridView};
            this.ControlModified?.Invoke(this, data);
        }

        /// <summary>
        ///     Adds the comment to the DataGridView.
        /// </summary>
        /// <param name="comment">The comment.</param>
        public void AddComment(string comment)
        {
            var rows = this.commentGraderGridView.RowCount;
            this.commentGraderGridView.Rows.Insert(rows - 1, false, comment);
        }

        /// <summary>
        ///     Builds the RadioButton group's text.
        /// </summary>
        public void BuildRadioButtonText()
        {
            this.exceptionalRadioButton.Text = $@"({this.ExceptionalValue}) Exceptional";
            this.acceptableRadioButton.Text = $@"({this.AcceptableValue}) Acceptable";
            this.amateurRadioButton.Text = $@"({this.AmateurValue}) Amateur";
            this.unsatisfactoryRadioButton.Text = $@"({this.UnsatisfactoryValue}) Unsatisfactory";
        }

        /// <summary>
        ///     Extracts the comments.
        /// </summary>
        /// <returns>A list of the checked comments.</returns>
        public string[] ExtractComments(bool checkedOnly)
        {
            var comments = new List<string>();
            foreach (DataGridViewRow row in this.commentGraderGridView.Rows)
            {
                if (checkedOnly)
                {
                    var cell = (DataGridViewCheckBoxCell) row.Cells[0];
                    var isCommentChecked = Convert.ToBoolean(cell.EditedFormattedValue);
                    findCheckedTextCell(isCommentChecked, row, comments);
                }
                else
                {
                    findTextCell(row, comments);
                }
            }
            return comments.ToArray();
        }

        /// <summary>
        ///     Finds the check button.
        /// </summary>
        /// <returns>The selected radio button</returns>
        public RadioButton FindCheckButton()
        {
            return this.gradeRadioButtonGroupBox.Controls.OfType<RadioButton>().FirstOrDefault(r => r.Checked);
        }

        /// <summary>
        ///     Finds the selected radio button value.
        /// </summary>
        /// <returns>The value of the selected radio button</returns>
        public int FindButtonChosenValue()
        {
            var pointsChecked = 0;
            var buttonChecked = this.FindCheckButton();

            if (buttonChecked.Tag.Equals("exceptional"))
            {
                pointsChecked = this.ExceptionalValue;
            }
            else if (buttonChecked.Tag.Equals("acceptable"))
            {
                pointsChecked = this.AcceptableValue;
            }
            else if (buttonChecked.Tag.Equals("amateur"))
            {
                pointsChecked = this.AmateurValue;
            }
            else
            {
                pointsChecked = this.UnsatisfactoryValue;
            }
            return pointsChecked;
        }

        /// <summary>
        ///     Checks or unchecks all the data grid view checkboxes based on parameter
        /// </summary>
        /// <param name="checkValue">if set to <c>true</c> [check value].</param>
        public void CheckAllStatus(bool checkValue)
        {
            foreach (DataGridViewRow row in this.commentGraderGridView.Rows)
            {
                if (row.Cells[1].Value != null)
                {
                    ((DataGridViewCheckBoxCell) row.Cells[0]).Value = checkValue;
                }
            }
        }

        /// <summary>
        ///     Resets the buttons to default (exceptional toggled).
        /// </summary>
        public void ResetButtons()
        {
            this.exceptionalRadioButton.Checked = true;
        }

        /// <summary>
        ///     Populates the text cells.
        /// </summary>
        /// <param name="comments">The comments.</param>
        public void PopulateTextCells(string[] comments)
        {
            this.removeAllComments();
            foreach (var currentComment in comments)
            {
                this.AddComment(currentComment);
            }
        }

        private void removeAllComments()
        {
            var rowCount = this.commentGraderGridView.RowCount - 1;
            for (var i = 0; i < rowCount; i++)
            {
                this.commentGraderGridView.Rows.RemoveAt(0);
            }
        }

        private static void findCheckedTextCell(bool isCommentChecked, DataGridViewRow row, List<string> comments)
        {
            if (isCommentChecked)
            {
                findTextCell(row, comments);
            }
        }

        private static void findTextCell(DataGridViewRow row, List<string> comments)
        {
            var textCell = (DataGridViewTextBoxCell) row.Cells[1];
            addExtractedComments(textCell, comments);
        }

        private static void addExtractedComments(DataGridViewTextBoxCell textCell, List<string> comments)
        {
            if (textCell.Value != null)
            {
                comments.Add(textCell.Value.ToString());
            }
        }

        private void uncheckAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.CheckAllStatus(false);
        }

        private void checkAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.CheckAllStatus(true);
        }

        private void deleteCellToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var selectedRow = this.commentGraderGridView.CurrentCell.RowIndex;
            this.commentGraderGridView.Rows.RemoveAt(selectedRow);
        }

        private void commentGraderGridView_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                this.checkAllContextMenu.Show(MousePosition);
            }
        }

        private void commentGraderGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            this.OnCommentGraderModified(this.commentGraderGridView);
        }

        private void exceptionalRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            this.OnCommentGraderModified(this.commentGraderGridView);
        }

        private void acceptableRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            this.OnCommentGraderModified(this.commentGraderGridView);
        }

        private void amateurRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            this.OnCommentGraderModified(this.commentGraderGridView);
        }

        private void unsatisfactoryRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            this.OnCommentGraderModified(this.commentGraderGridView);
        }

        /// <summary>
        ///     Inherits from EventArgs, Used to handle the arguments for when the control has been modified.
        /// </summary>
        /// <seealso cref="System.EventArgs" />
        public class ControlModifiedEventArgs : EventArgs
        {
            #region Properties

            /// <summary>
            ///     Gets or sets the comment data grid view.
            /// </summary>
            /// <value>
            ///     The comment data grid view.
            /// </value>
            public DataGridView CommentDataGridView { get; set; }

            #endregion
        }

        #endregion
    }
}